//
//  ForecastCell.swift
//  weatherSecond
//
//  Created by 이요한 on 24/07/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import UIKit

class ForecastCell: UITableViewCell {

    @IBOutlet var forecastMaxTemp: UILabel!
    @IBOutlet var forecastMinTemp: UILabel!
    @IBOutlet var forecastDay: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(forecastData: ForecastWeather ) {
        self.forecastDay.text = "\(forecastData.date)"
        self.forecastMaxTemp.text = "\(Int(forecastData.maxTemp))"
        self.forecastMinTemp.text = "\(Int(forecastData.minTemp))"
    }

}
