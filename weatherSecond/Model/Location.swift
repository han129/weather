//
//  Location.swift
//  weatherSecond
//
//  Created by 이요한 on 23/07/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import Foundation

class Location {
    
    static var sharedInstance = Location()
    
    var longitude: Double!
    var latitude: Double!
}
