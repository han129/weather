//
//  ForecastWeather.swift
//  weatherSecond
//
//  Created by 이요한 on 25/07/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import Foundation

class ForecastWeather {
    private var _date: String!
    private var _minTemp: Double!
    private var _maxTemp: Double!
    
    var date: String {
        if _date == nil {
            _date = ""
        }
        return _date
    }
    
    var minTemp: Double {
        if _minTemp == nil {
            _minTemp = 0.0
        }
        return _minTemp
    }
    
    var maxTemp: Double {
        if _maxTemp == nil {
            _maxTemp = 0.0
        }
        return _maxTemp
    }
    
    init(weatherDic: Dictionary<String, AnyObject>) {
            if let day = weatherDic["time"] as? Double {
                
//                let converDate = Date(timeIntervalSince1970: day)
//                let dateFormatter = DateFormatter()
//                dateFormatter.dateStyle = .full
//                dateFormatter.timeStyle = .none
//                dateFormatter.locale = Locale(identifier: "ko")
//                let dailyDate = dateFormatter.string(from: converDate)
//
//                self._date = "\(dailyDate)"
                
                let rawDate = Date(timeIntervalSince1970: day)
                let dateFormatter = DateFormatter()
                dateFormatter.dateStyle = .medium
                dateFormatter.dateFormat = "EEEE"
                dateFormatter.locale = Locale(identifier: "ko")
                let dailyDate = dateFormatter.string(from: rawDate)
                
                self._date = "\(dailyDate)"
            }
        
        if let minDaytemp = weatherDic["temperatureMin"] as? Double {
            self._minTemp = (minDaytemp - 32)*5/9
            
        }
    
        if let maxDaytemp = weatherDic["temperatureMax"] as? Double {
            self._maxTemp = (maxDaytemp - 32)*5/9
            
        
    }
        
        
    }
    
    
    
}


