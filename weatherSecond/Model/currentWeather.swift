//
//  currentWeather.swift
//  weatherSecond
//
//  Created by 이요한 on 22/07/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class CurrentWeather {
    
    private var _cityName: String!
    private var _date: String!
    private var _weatherType: String!
    private var _currentTemp: Double!
    
    var cityName: String {
        if _cityName == nil {
            _cityName = ""
        }
        return _cityName
    }
    
    var date: String {
        if _date == nil {
           _date = ""
        }
        return _date
    }
    
    var weatherType: String {
        if _weatherType == nil {
            _weatherType = ""
        }
        return _weatherType
    }
    
    var currentTemp: Double {
        if _currentTemp == nil {
           _currentTemp = 0.0
        }
        return _currentTemp
    }
    
    
    func downloadCurrentWeather(completed: @escaping downloadComplete) {
        Alamofire.request(API_URL).responseJSON{(response) in
            let result = response.result
            let json = JSON(result.value!)
            
            print(result)
            
            self._cityName = json["timezone"].stringValue
            print(self._cityName!)
            
            self._weatherType = json["currently"]["summary"].stringValue
            print(self._weatherType!)
            
            let currently = json["currently"]
            let tempData = currently["time"].double
            let converDate = Date(timeIntervalSince1970: tempData!)
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .full
            dateFormatter.timeStyle = .none
            dateFormatter.locale = Locale(identifier: "ko")
            let currentDate = dateFormatter.string(from: converDate)
            self._date = " \(currentDate)"
            print(self._date!)
            
            let downloadedTemp = json["currently"]["temperature"].double
            print(downloadedTemp!)
            self._currentTemp = (downloadedTemp! - 32)*5/9
            print(self._currentTemp!)
            completed()
        }
       
    }
    
}
