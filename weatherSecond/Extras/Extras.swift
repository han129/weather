//
//  Extras.swift
//  weatherSecond
//
//  Created by 이요한 on 22/07/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import Foundation

let API_URL = "https://api.darksky.net/forecast/b8ad1b7330360378cdffb8325ebe668d/\(Location.sharedInstance.latitude!),\(Location.sharedInstance.longitude!)"
 

typealias downloadComplete = () -> ()
