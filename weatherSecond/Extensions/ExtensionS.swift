//
//  ExtensionS.swift
//  weatherSecond
//
//  Created by 이요한 on 22/07/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import Foundation


extension Double {
    func rounded(toPlaces Places:Int) -> Double {
        let divisor = pow(10.0, Double(Places))
        return (self * divisor).rounded() / divisor
    }
}


//extension Data {
//    func dayOfTheWeek() -> String {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "EEEE"
//        return dateFormatter.string(from: self)
//    }
//} part 2 25:36


